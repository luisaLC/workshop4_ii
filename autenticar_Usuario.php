<?php
  require('functions.php');

  session_start();

  if($_POST) {
    $usuarioN = $_REQUEST['usuarioN'];
    $contrasena = $_REQUEST['contrasena'];

    $usuario = authenticate($usuarioN, $contrasena);
        
        if($usuario){
          $_SESSION['usuario'] = $usuario;
            
            if($usuario['tipoUsuario'] === 'admin'){
                
                header('Location: /WorkShop4/perfil.php?status=login');
                
            }else{
                
                  header('Location: /WorkShop4/col.php?status=login');
            }
        }
      
  }else{
      header('Location: /WorkShop4/error.php');
  }
  