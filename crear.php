<?php
  include('functions.php');

  if(isset($_POST['nombre']) && isset($_POST['apellido']) && isset($_POST['usuarioN']) 
    && isset($_POST['contrasena']) && isset($_POST['tipoUsuario'])) {
      
        $saved = saveUsuario($_POST);

        if($saved) {
          header('Location: /WorkShop4/?status=success');
        } else {
          header('Location: /WorkShop4/error.php');
        }
  } else {
    header('Location: /WorkShop4/error.php');
  }