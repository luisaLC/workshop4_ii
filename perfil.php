<?php
  // get all users from the database
  $sql = 'SELECT * FROM usuario';
  $connection = new mysqli('localhost:3306','root','lulitrix','login');
  $result = $connection->query($sql);
  $users = $result->fetch_all();
?>

<?php
session_start();

$usuario = $_SESSION['usuario'];
if(!$usuario){
    header('Location: /WorkShop4/perfil.php');
}

?>

<?php
  $message = "";
  if(!empty($_REQUEST['status'])) {
    switch($_REQUEST['status']) {
      case 'success':
        $message = 'User was added succesfully';
      break;
      case 'error':
        $message = 'There was a problem inserting the user';
      break;
    }
  }
?>

<!DOCTYPE html>
<html lang="es">

    <head>
        <meta charset="utf-8">
        <title>Login con tipo de usuario</title>
        </head>
    <body>
        <h1> Bienvenido <?php echo $usuario ['nombre']; echo $usuario ['apellido']; 
            ?></h1>
            
        <h1>Crear Usuario</h1>
        
        <form action="/WorkShop4/crear.php" method="POST" class="form-inline" role="form">
          <div class="form-group">
            <label class="sr-only" for="">Usuario</label>
            <input type="text" class="form-control" id="" name="usuarioN" placeholder="Your username">
          </div>
          <div class="form-group">
            <label class="sr-only" for="">Nombre</label>
            <input type="text" class="form-control" id="" name="nombre" placeholder="Your Name">
          </div>
          <div class="form-group">
            <label class="sr-only" for="">Apellido</label>
            <input type="text" class="form-control" id="" name="apellido" placeholder="Your LastName">
          </div>
          <div class="form-group">
            <label class="sr-only" for="">Contraseña</label>
            <input type="text" class="form-control" id="" name="contrasena" placeholder="Your LastName">
          </div>
          <div class="form-group">
            <label class="sr-only" for="">Tipo</label>
            <input type="text" class="form-control" id="" name="tipoUsuario" placeholder="Your LastName">
          </div>

          <input type="submit" class="btn btn-primary" value="Submit">
        </form> 
        
        <div class="container">
              <h1>Lista de Usuarios</h1>
                <table class="table table-light">
                  <tr>
                    <th>Nombre</th>
                    <th>Apellido</th>
                    <th>Actions</th>
                  </tr>
                  <tbody>
                    <?php
                      // loop users
                        foreach($users as $user) {
                          echo "<tr>
                          <td>".$user[1]."</td>
                          <td>".$user[2]."</td>
                          <td><a href=\"edit.php?id=".$user[0]."\">Edit</a> | 
                              <a href=\"delete.php?id=".$user[0]. "\">Delete</a></td>
                        </tr>";
                        }
                    ?>
                  </tbody>
                </table>
                <?php

              $connection->close();
            ?>
        </div> 
          
            
        <a href="/WorkShop4/logOut.php"> Cerrar Sesion </a>
        
        <nav class nav></nav>
    </body>
</html>